### 嗨 你好啊 👋

- 🔭 一名计算机科学技术----深度学习---多模态方向研究生
- 🌱 I’m currently learning c,python,Kotlin,kali linux
- 👯 I’m looking to collaborate on openwrt
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: .qq
- 😄 Pronouns: ...
- ⚡ Fun fact: ...

docker pull condaforge/miniforge3

docker run -it condaforge/miniforge3 /bin/bash

conda create -n myenv python=3.7

conda activate myenv

conda install numpy

以下为阅读的论文目录；










### 1.METER

标题：An Empirical Study of Training End-to-End Vision-and-Language Transformers

来源：CVPR 2022

代码：zdou0830/METER: METER: A Multimodal End-to-end TransformER Framework (github.com)，

文：https://arxiv.org/abs/2111.02387

关键词：VL

代码与数据集：

概述：本代码基于clip框架进行改进

clip框架： https://gitcode.com/gh_mirrors/cl/CLIP/overview

### 2.DVTOD CMA-Det

标题：基于无人机构建了一个未对齐的RGB-T目标检测**数据集**和跨模态对齐**检测器**

英文标题：Misaligned Visible-thermal Object Detection: A Drone-based Benchmark and Baseline

论文地址：https://ieeexplore.ieee.org/document/10522939

关键词：dvtod模型 V-T 目标检测

代码：https://github.com/2926295173/neu-two https://github.com/VDT-2048/DVTOD

环境：py3.7 

概述：基于yolov5改进，需要先熟悉yolov5

### 3.CLIP

标题：Learning Transferable Visual Models From Natural Language Supervision

博客文：https://openai.com/index/clip/

文章源：https://arxiv.org/abs/2103.00020

代码：https://gitcode.com/gh_mirrors/cl/CLIP/overview

关键词：VL



### 4.DMR

标题：DMR: Decomposed Multi-Modality Representations for Frames and Events Fusion in Visual Reinforcement Learning

文章下载：
https://openaccess.thecvf.com/content/CVPR2024/papers/Xu_DMR_Decomposed_Multi-Modality_Representations_for_Frames_and_Events_Fusion_in_CVPR_2024_paper.pdf

https://openaccess.thecvf.com/content/CVPR2024/supplemental/Xu_DMR_Decomposed_Multi-Modality_CVPR_2024_supplemental.pdf

CODE:https://github.com/kyoran/DMR

关键词：RL RGB-DVS










TransFuser

EFNet

FPNet

RENet







